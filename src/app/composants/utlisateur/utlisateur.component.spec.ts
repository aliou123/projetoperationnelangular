import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UtlisateurComponent } from './utlisateur.component';

describe('UtlisateurComponent', () => {
  let component: UtlisateurComponent;
  let fixture: ComponentFixture<UtlisateurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UtlisateurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UtlisateurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
