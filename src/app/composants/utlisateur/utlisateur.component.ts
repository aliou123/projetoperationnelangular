import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-utlisateur',
  templateUrl: './utlisateur.component.html',
  styleUrls: ['./utlisateur.component.css']
})
export class UtlisateurComponent implements OnInit {
  nom: string;
  age: number;
  email: string;
  hobby: hobby;

  constructor() { }

  ngOnInit() {
    this.nom = 'Aliou';
    this.age = 32;
    this.email = 'ndiayealiou99@gmail.com';
    this.hobby = {
      hobbyUn: 'Lire',
      hobbyDeux: 'Basket',
      hobbyTrois: 'Apprendre'
    };
  }
  onClick() {
    alert('Aucune information supplémentaire pour le moment !');
  }
}

interface hobby {
  hobbyUn: string;
  hobbyDeux: string;
  hobbyTrois: string;
}