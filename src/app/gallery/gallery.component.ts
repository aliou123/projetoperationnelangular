import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
  pagePhotos:any;
  constructor(private http:HttpClient) { }

  ngOnInit() {
  }
  onSearch(dataForm){
    this.http.get("https://pixabay.com/api/?key=11971303-63842cf457125afa81cf232bd&q="+dataForm.motCle+"&per_page=10&page=1").subscribe(data =>{this.pagePhotos=data;})
  }

}
